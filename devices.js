var vendormodels = {
  "8devices": {
    "Carambola Board 2": "8devices-carambola2-board"
  },

  "Alfa": {
    "AP121": "ap121",
    "AP121U": "ap121u",
    "Hornet-UB": "hornet-ub",
    "Tube2H": "tube2h",
    "N2 / N5": "n2-n5"
  },

  "Allnet": {
    "ALL0315N": "all0315n"
  },

  "Buffalo": {
    "WZR-600DHP": {"wzr-600dhp": "v2"},
    "WZR-HP-AG300H": {"wzr-hp-ag300h": "", "wzr-hp-ag300h-wzr-600dhp": ""},
    "WZR-HP-G300H": {"wzr-hp-g300h": ""},
    "WZR-HP-G300NH": {"wzr-hp-g300nh": "v1", "wzr-hp-g300nh2": "v2"},
    "WZR-HP-G450H": {"wzr-hp-g450h": ""},
  },

  "D-Link": {
    "DIR-505": "dir-505",
    "DIR-615": {"dir-615-c1": "c1/c2", "dir-615-rev-c1": "c1/c2", "dir-615-d": "d1-d4", "dir-615-e1": "e1/e2", "dir-615-e4": "e3-e5", "dir-615-h1": "h1/h2"},
    "DIR-825": "dir-825",
    "DIR-860L": "dir-860l"
  },

  "GL Innovations": {
    "AR150": "ar150",
    "AR300M": "ar300m",
    "iNet 6408a": "inet-6408a",
    "iNet 6416a": "inet-6416a",
    "GL-MT300a": "mt300a",
    "GL-MT300n": "mt300n",
    "GL-MT750": "mt750"
  },

  "LeMaker": {
    "Banana Pi": "banana-pi",
    "Banana Pro": "banana-pro",
    "Banana Pi R1 (Lamobo)": {"lamobo-r1": "", "lamobo": "", "lemaker-lamobo-r1": ""}
  },

  "Linksys": {
    "WRT160NL": "wrt160nl",
    "WRT1200AC": "wrt1200ac"
  },

  "Meraki": {
    "MR12": "mr12",
    "MR16": "mr16",
    "MR62": "mr62",
    "MR66": "mr66"
  },

  "Mikrotik": {
    "vmlinux": {"mikrotik-vmlinux-lzma" : "", "mikrotik-vmlinux.lzma" : ""},
    "nand": {"mikrotik-nand-64m" : "64m", "mikrotik-nand-large" : "large"}
  },

  "Netgear": {
    "WNR2200": "wnr2200",
    "WNDR3700": {"wndr3700": "v1", "wndr3700v2": "v2", "wndr3700v3": "v3", "wndr3700v4": "v4"},
    "WNDR3800": "wndr3800",
    "WNDR4300": "wndr4300",
    "WNDRMAC": {"wndrmac": "v1", "wndrmacv2": "v2"},
  },

  "Onion": {
    "Omega": "onion-omega"
  },

  "Open-Mesh": {
    "MR1750": {"openmesh-mr1750": "v1", "openmesh-mr1750v2": "v2"},
    "MR600": {"openmesh-mr600": "v1", "openmesh-mr600v2": "v2"},
    "MR900": {"openmesh-mr900": "v1", "openmesh-mr900v2": "v2"},
    "OM2P HS": {"openmesh-om2p-hs": "v1", "openmesh-om2p-hsv3": "v3"},
    "OM2P LC": "openmesh-om2p-lc",
    "OM2P": {"openmesh-om2p": "v1", "openmesh-om2pv2": "v2"},
    "OM5P-AC": {"openmesh-om5p-ac": "v1", "openmesh-om5p-acv2": "v2"},
    "OM5P-AN": "openmesh-om5p-an",
    "OM5P": "openmesh-om5p"
  },

  "PC Engines": {
    "ALIX": "x86-geode"
  },

  "Raspberry Pi Foundation": {
    "PI": "raspberry-pi",
    "PI 2": "raspberry-pi-2",
    "PI 3": "raspberry-pi-3"
  },

  "TP-Link": {
    "Archer C2600": {"archer-c2600": "v1"},
    "Archer C20": {"ArcherC20i": "", "tplink_c20": "v4"},
    "Archer C25": {"archer-c25": ""},
    "Archer C50": {"ArcherC50": "v1", "tplink_c50": "v3"},
    "Archer C59": {"archer-c59": ""},
    "Archer C60": {"archer-c60": ""},
    "Archer C5": {"archer-c5": ""},
    "Archer C7": {"archer-c7": ""},
    "CPE 210": {"cpe210": "v1 und v1.1", "cpe210-220": "v1 und v1.1"},
    "CPE 510": {"cpe510": "v1 und v1.1", "cpe510-520": "v1 und v1.1"},
    "WBS 210": "wbs210",
    "WBS 510": "wbs510",
    "RE450": "re450",
    "TL-MR13U": "tl-mr13u",
    "TL-MR3020": "tl-mr3020",
    "TL-MR3040": "tl-mr3040",
    "TL-MR3220": "tl-mr3220",
    "TL-MR3420": {"tl-mr3420": "", "tplink_tl-mr3420": ""},
    "TL-WA701N/ND": {"tl-wa701nd": "", "tl-wa701n-nd": ""},
    "TL-WA7210N/ND": "tl-wa7210n",
    "TL-WA730RE": "tl-wa730re",
    "TL-WA750RE": "tl-wa750re",
    "TL-WA7510N": "tl-wa7510n",
    "TL-WA801N/ND": {"tl-wa801n": "", "tl-wa801n-nd": ""},
    "TL-WA830RE": "tl-wa830re",
    "TL-WA850RE": "tl-wa850re",
    "TL-WA860RE": "tl-wa860re",
    "TL-WA901N/ND": {"tl-wa901nd": "", "tl-wa901n-nd": ""},
    "TL-WDR3500": "tl-wdr3500",
    "TL-WDR3600": "tl-wdr3600",
    "TL-WDR4300": "tl-wdr4300",
    "TL-WDR4900": "tl-wdr4900",
    "TL-WR1043N/ND": {"tl-wr1043n-nd": "", "tl-wr1043n": "", "tl-wr1043nd": "", "tl-wr1043": ""},
    "TL-WR2543N/ND": {"tl-wr2543n-nd": ""},
    "TL-WR703N": "tl-wr703n",
    "TL-WR710N": "tl-wr710n",
    "TL-WR740N/ND": "tl-wr740n-nd",
    "TL-WR741N/ND": "tl-wr741n-nd",
    "TL-WR743N/ND": "tl-wr743n-nd",
    "TL-WR840N/ND": {"tl-wr840n-nd": "", "tl-wr840nd": "", "tl-wr840n": "",  "tl-wr840": ""},    
    "TL-WR841N/ND": {"tl-wr841n-nd": "", "tl-wr841nd": "", "tl-wr841n": "",  "tl-wr841": ""},
    "TL-WR842N/ND": {"tl-wr842n-nd": "",  "tl-wr842n": "v3"},
    "TL-WR843N/ND": {"tl-wr843n-nd": ""},
    "TL-WR902ac": {"tplink_tl-wr902ac": ""},
    "TL-WR940N/ND": {"tl-wr940n": "", "tl-wr940n-nd": "", "tl-wr941nd-v4": "v1", "tl-wr941nd-v5": "v2", "tl-wr941nd-v6": "v3"},
    "TL-WR941N/ND": {"tl-wr941nd": "", "tl-wr941n-nd": "", "tl-wr941nd-v4": "v4", "tl-wr941nd-v5": "v5", "tl-wr941nd-v6": "v6"}
  },

  "Ubiquiti": {
    "AirGateway": {"ubnt-air-gateway": "", "ubiquiti-airgateway": "",
                   "ubnt-air-gateway-pro": "", "ubiquiti-airgateway-pro": "pro"},
    "AirRouter": {"ubnt-air-router": "", "ubnt-airrouter": ""},
    "Bullet": {"bullet-m2": "M2", "bullet-m5": "M5", "ubnt-bullet-m": "M2/M5"},
    "Litestation SR71": "ls-sr71",
    "NanoStation Loco": {"loco-m-xw": "M2/M5 XW", "loco-m2": "M2 XM", "loco-m5": "M5 XM", "loco-m2-xw": "M2 XW", "loco-m5-xw": "M5 XW"},
    "NanoStation": {"nanostation-m": "M2/M5 XM", "ubnt-nano-m": "M2/M5 M", "nanostation-m-xw": "M2/M5 XW", "ubnt-nano-m-xw": "M2/M5 XW",
                    "nanostation-m2": "M2 XM", "nano-m2": "M2 XM", "nanostation-m2-xw": "M2 XW", "nano-m2-xw": "M2 XW",
                    "nanostation-m5": "M5 XM", "nano-m5": "M5 XM", "nanostation-m5-xw": "M5 XW", "nano-m5-xw": "M5 XW"},
    "PicoStation": "picostation",
    "Rocket": {"rocket-m-xw": "M XW", "rocket": "M XM"},
    "UniFi AP": "unifi",
    "UniFi AP LR": "unifi-ap-lr",
    "UniFi AP Pro": "unifi-ap-pro",
    "UniFi AP AC Pro": "unifi-ac-pro",
    "UniFi AP AC Lite": "unifi-ac-lite",
    "UniFi AP Outdoor": {"unifi-outdoor": "", "unifiap-outdoor": ""},
    "UniFi AP Outdoor+": {"unifi-outdoor-plus": "", "ubiquiti-unifiap-outdoor+": "", "ubiquiti-unifiap-outdoor%2B": ""}
  },

  "Unbranded": {
    "A5-V11": "a5-v11"
  },

  "VoCore": {
    "VoCore": "vocore",
    "VoCore 2": "vocore2"
  },

  "Western Digital": {
    "My Net N600": {"my-net-n600": ""},
    "My Net N750": {"my-net-n750": ""}
  },

  "Xiaomi": {
    "MiWiFi Mini": "miwifi-mini"
  },

  "x86": {
    "generic 32bit": "x86-generic",
    "generic 64bit": "x86-64",
    "VirtualBox 32bit": {"x86-virtualbox.vdi": "", "x86-generic.vdi": ""},
    "VirtualBox 64bit": {"x86-64-virtualbox.vdi": "", "x86-64.vdi": ""},
    "VMware 32bit": {"x86-vmware.vmdk": "", "x86-generic.vmdk": ""},
    "VMware 64bit": {"x86-64-vmware.vmdk": "", "x86-64-generic.vmdk": "",
                     "x86-64.vmdk": ""},
    "KVM 32bit": "x86-kvm",
    "XEN 32bit": "x86-xen",
  }
};
