/*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

var config = {
  // list images on console that match no model
  listMissingImages: true,
  // see devices.js for different vendor model maps
  vendormodels: vendormodels,
  // community prefix of the firmware images
  community_prefix: 'ffbsee-',
  // firmware version regex
  version_regex: '-([0-9]+.[0-9]+.[0-9]+([+-~][0-9]+)?)[.-]',
  // relative image paths and branch
  directories: {
    '../firmware/1.0.5/ar71xx/': 'stable',
    '../firmware/1.0.5/mpc85xx/': 'stable',
    '../firmware/1.0.3/ar71xx/': 'oldstable',
    '../firmware/1.0.3/mpc85xx/': 'oldstable',
    '../firmware/1.1.1/ar71xx/generic/': 'beta',
    '../firmware/1.1.1/ramips/mt76x8/': 'beta',
  },
  // path to preview pictures directory
  preview_pictures: 'pictures/',
  // link to changelog
  changelog: 'CHANGELOG.html'
};
